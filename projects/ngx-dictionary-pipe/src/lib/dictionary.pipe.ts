import { Pipe, PipeTransform } from '@angular/core';

// Author: José Francisco Martínez Rangel © 2019
// tslint:disable-next-line:use-pipe-transform-interface
@Pipe({ name: 'dictionary' })
export class DictionaryPipe implements PipeTransform {
  private readonly BAD_PATH_ERROR = [];
  private readonly BAD_KEY_ERROR = [];
  private readonly VALUE_NOT_FOUND_ERROR = [];

  /**
   * Searches in a dictionary for a matching item based in a key value, to provide a readable name from it.
   * @param value Value to search within the dictionary items.
   * @param dictionary Dictionary to search in.
   * @param path Path to follow before searching.
   * @param nameKey Name property to return from the final object. Will return final object if not specified.
   */
  transform(value: any, dictionary: any, path: any | any[], nameKey?: string, debug?: boolean) {
    if (value === undefined || value === null || value === '') {
      return ''; // probably this would be the intended behaviour (if undefined, null or empty return '')
    }
    if (!path) {
      return '?'; // bad path
    }

    if (typeof path === 'string') {
      // a.path.like.this was received, split it
      path = path.split('.');
    } else {
      // clone array in case it should not be modified
      path = path.slice();
    }

    // check if it's a raw array/map access
    if (path[0]) {
      switch (path[0][0]) {
        case '[':
          path.splice(0, 1, 'pa[]'); // pseudo-array
          dictionary = {
            pa: dictionary,
          };
          break;
        case '{':
          path.splice(0, 1, 'pm{}'); // pseudo-array
          dictionary = {
            pm: dictionary,
          };
          break;
        default:
          break; // no patch needed
      }
    }

    /// console.log('INPUT PATH:');
    /// console.log(path);

    // take value keys from the last parameter (do not take if it's the only path key!)

    const valueKey = path.length > 1 ? path.pop() : undefined;

    // TODO: In v2 or v3, remove requirement of this inference patch
    // add inference patch (e.g. books.id -> books[].id)
    const patchPathKey = path[path.length - 1];
    if (patchPathKey) {
      switch (patchPathKey[patchPathKey.length - 1]) {
        case ']':
          // check if not empty [] last step, since it cannot match it anymore
          if (patchPathKey[patchPathKey.length - 2] === '[') {
            path.splice(path.length - 1, 1, path[path.length - 1].replace('[]', valueKey ? `[${valueKey}:${value}]` : `[${value}]`));
          } else {
            // patch not required
          }
          break;
        case '}':
          // check if not empty {} last step, since it cannot match it anymore
          if (patchPathKey[patchPathKey.length - 2] === '{') {
            path.splice(path.length - 1, 1, path[path.length - 1].replace('{}', valueKey ? `{${valueKey}:${value}}` : `{${value}}`));
          } else {
            // patch not required
          }
          break;
        default:
          path.splice(path.length - 1, 1, path[path.length - 1] + `[${valueKey}:${value}]`);
          break;
      }
      /// console.log(path.length);

      /// console.log(
      ///   `{{ ${value} | dictionary : masterData : [${path
      ///     .concat(patchPathKey)
      ///     .map(x => `'${x}'`)
      ///     .join(`, `)}]` + (nameKey ? ` : '${nameKey}' }}` : '')
      /// );
    }

    dictionary = this.navigateToPath(dictionary, path);

    /// console.log('FINAL RESULT:');
    /// console.log(dictionary);

    switch (dictionary) {
      case this.BAD_PATH_ERROR:
        return debug ? '?' : '';
      case this.BAD_KEY_ERROR:
        return debug ? '??' : '';
      case this.VALUE_NOT_FOUND_ERROR:
        return debug ? '???' : '';
      default:
        if (!nameKey) {
          // no property requested, return object
          return dictionary;
        }
        if (dictionary[nameKey] === undefined) {
          // for an unknown reason, value key was not found
          return '???';
        } else {
          // found value, return it
          return dictionary[nameKey];
        }
    }
  }

  /**
   * Performs navigation to an specified path, returning the destination if found, error if failed.
   * @param dictionary Dictionary to transverse through
   * @param remainingPath Path pending to transverse (this function is recursive)
   */
  private navigateToPath(dictionary, remainingPath: string[]) {
    if (remainingPath == null || remainingPath.length === 0) {
      return dictionary;
    }

    if (dictionary) {
      /// console.log('TARGET:');
      /// console.log(dictionary);
      /// console.log('REMAINING PATH: ');
      /// console.log(remainingPath);
      const path = remainingPath[0];
      /// console.log(path);
      switch (path[path.length - 1]) {
        // lookup in dictionary
        case ']': {
          // navigate to next target
          const pretargetKey = path.split('[')[0];
          if (dictionary[pretargetKey] == null) {
            return this.BAD_PATH_ERROR;
          }
          dictionary = dictionary[pretargetKey];

          // get [value inside brackets]
          let valueKey = '';
          let value = path.substr(pretargetKey.length + 1, path.length - pretargetKey.length - 2);

          // if value has 'key:value', separate them
          if (value.includes(':')) {
            const tuple = value.split(':');
            valueKey = tuple[0];
            value = tuple[1];
          }

          let possibleBadKey = false;
          if (valueKey !== '') {
            // collect support
            let collectDictionary = null;
            let collectArray = null;
            /// console.log(remainingPath.length);
            if (remainingPath.length === 1) {
              if (valueKey.endsWith('[]')) {
                collectArray = [];
                valueKey = valueKey.substr(0, valueKey.length - 2);
              } else if (valueKey.endsWith('{}')) {
                collectDictionary = {}; // TODO: Implement
                valueKey = valueKey.substr(0, valueKey.length - 2);
              }
            }

            // find object with .key = value
            // tslint:disable-next-line:forin
            for (const targetKey in Object.keys(dictionary || {})) {
              /// console.log('TARGET:');
              /// console.log(`target[${targetKey}]`);
              // tslint:disable-next-line:triple-equals
              /// console.log(`target[${valueKey}] == ${value} --> ${dictionary[targetKey][valueKey] == value}`);
              // check if not null target, else, ignore it
              if (!dictionary[targetKey]) {
                continue;
              }

              // key is missing, possible typo if no result found
              if (dictionary[targetKey][valueKey] === undefined) {
                possibleBadKey = true;
              }

              // softened comparison for stringed id
              // tslint:disable-next-line:triple-equals
              if (dictionary[targetKey][valueKey] == value) {
                // collect support
                if (collectArray) {
                  collectArray.push(dictionary[targetKey]);
                } else {
                  return this.navigateToPath(dictionary[targetKey], remainingPath.slice(1));
                }
              }
            }
            /// console.log(valueKey);

            /// console.log(collectArray);

            // collect support
            if (collectArray && collectArray.length === 0) {
              collectArray = null;
            } else if (collectDictionary && Object.keys(collectDictionary).length === 0) {
              collectDictionary = null;
            }

            // for an unknown reason, item was not found in the dictionary
            return possibleBadKey ? this.BAD_KEY_ERROR : collectArray || collectDictionary || this.VALUE_NOT_FOUND_ERROR;
          } else if (value !== '') {
            // find object at index [value]
            /// console.log(`Looking for target[${value}]`);
            return this.navigateToPath(dictionary[value], remainingPath.slice(1));
          } else {
            // make a loop to try and find target
            /// console.log(`Looping targets`);
            for (const target of dictionary) {
              const result = this.navigateToPath(target, remainingPath.slice(1));
              switch (result) {
                case this.BAD_KEY_ERROR:
                case this.BAD_PATH_ERROR:
                case this.VALUE_NOT_FOUND_ERROR:
                  continue;
                default:
                  return result;
              }
            }
            // for an unknown reason, item was not found in the dictionary
            return this.VALUE_NOT_FOUND_ERROR;
          }
        }
        case '}': {
          // navigate to next target
          const pretargetKey = path.split('{')[0];
          if (dictionary[pretargetKey] == null) {
            return this.BAD_PATH_ERROR;
          }
          dictionary = dictionary[pretargetKey];

          // get [value inside brackets]
          let valueKey = '';
          let value = path.substr(pretargetKey.length + 1, path.length - pretargetKey.length - 2);

          // if value has 'key:value', separate them
          if (value.includes(':')) {
            const tuple = value.split(':');
            valueKey = tuple[0];
            value = tuple[1];
          }

          let possibleBadKey = false;
          if (valueKey !== '') {
            // collect support
            let collectDictionary = null;
            let collectArray = null;
            if (remainingPath.length === 1) {
              if (valueKey.endsWith('[]')) {
                collectArray = [];
                valueKey = valueKey.substr(0, valueKey.length - 2);
              } else if (valueKey.endsWith('{}')) {
                collectDictionary = {}; // TODO: Implement
                valueKey = valueKey.substr(0, valueKey.length - 2);
              }
            }

            // find object with .key = value
            // tslint:disable-next-line:forin
            for (const targetKey of Object.keys(dictionary || {})) {
              /// console.log('TARGET:');
              /// console.log(dictionary);
              /// console.log(`target[${targetKey}]`);
              // tslint:disable-next-line:triple-equals
              /// console.log(`target[${targetKey}][${valueKey}] == ${value} --> ${dictionary[targetKey] && dictionary[targetKey][valueKey] == value}`);
              // check if not null target, else, ignore it
              if (!dictionary[targetKey]) {
                continue;
              }

              // key is missing, possible typo if no result found
              if (dictionary[targetKey][valueKey] === undefined) {
                possibleBadKey = true;
                continue;
              }

              // softened comparison for stringed id
              // tslint:disable-next-line:triple-equals
              if (dictionary[targetKey][valueKey] == value) {
                // collect support
                if (collectArray) {
                  collectArray.push(dictionary[targetKey]);
                } else {
                  return this.navigateToPath(dictionary[targetKey], remainingPath.slice(1));
                }
              }
            }

            // collect support
            if (collectArray && collectArray.length === 0) {
              collectArray = null;
            } else if (collectDictionary && Object.keys(collectDictionary).length === 0) {
              collectDictionary = null;
            }

            // for an unknown reason, item was not found in the dictionary
            return possibleBadKey ? this.BAD_KEY_ERROR : collectArray || collectDictionary || this.VALUE_NOT_FOUND_ERROR;
          } else if (value !== '') {
            // find object at index [value]
            /// console.log(`Looking for target[${value}]`);
            return this.navigateToPath(dictionary[value], remainingPath.slice(1));
          } else {
            // make a loop to try and find target
            /// console.log(`Looping targets`);
            for (const targetKey of Object.keys(dictionary)) {
              const result = this.navigateToPath(dictionary[targetKey], remainingPath.slice(1));
              switch (result) {
                case this.BAD_KEY_ERROR:
                case this.BAD_PATH_ERROR:
                case this.VALUE_NOT_FOUND_ERROR:
                  continue;
                default:
                  return result;
              }
            }
            // for an unknown reason, item was not found in the dictionary
            return this.VALUE_NOT_FOUND_ERROR;
          }
        }
        default:
          return this.navigateToPath(dictionary[path], remainingPath.slice(1));
      }
    } else {
      // got no target
      return this.BAD_PATH_ERROR;
    }
  }
}
