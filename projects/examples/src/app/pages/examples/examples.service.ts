import { Injectable } from '@angular/core';

type ExampleTypeKey = 'dictionary' | 'masterData' | 'complex';

export interface ExampleType {
  key: ExampleTypeKey;
  label: string;
}

export interface ExampleData {
  type: ExampleTypeKey;
  searchValue: string;
  path: string;
  inputDictionary: any;
  outputKey?: string;
  description: string;
  flow: string;
}

@Injectable({
  providedIn: 'root'
})
export class ExamplesService {
  private exampleTypes: ExampleType[] = [
    {
      key: 'dictionary',
      label: 'Using a dictionary',
    },
    {
      key: 'masterData',
      label: 'Using a master data',
    },
    {
      key: 'complex',
      label: 'Using a complex data source',
    }
  ];

  private booksDictionary = [
    {
      "id": 1,
      "name": "The dawn of the night",
      "group": "dawn",
    },
    {
      "id": 2,
      "name": "The dawn of the morning",
      "group": "dawn",
    },
    {
      "id": 3,
      "name": "Crown and Jewel",
      "group": "dawn",
    },
  ];
  private booksMasterData = {
    books: this.booksDictionary,
  }
  private examples: ExampleData[] = [
    {
      type: 'dictionary',
      searchValue: '1',
      path: '[].id',
      inputDictionary: this.booksDictionary,
      outputKey: `'name'`,
      description: `Lookup object by ID in an array, and return it's 'name' property.`,
      flow: `1 => 'Name'`,
    },
    {
      type: 'dictionary',
      searchValue: '1',
      path: '[].id',
      inputDictionary: this.booksDictionary,
      outputKey: `object`,
      description: `Lookup object by ID in an array, and return it.`,
      flow: `1 => object`,
    },
    {
      type: 'dictionary',
      searchValue: 'dawn',
      path: '[].group[]',
      inputDictionary: this.booksDictionary,
      outputKey: `array`,
      description: `Lookup objects by group ID in an array, collect them and return them.`,
      flow: `'dawn' => [object1, object2, ...] where object.group == 'dawn'`,
    },
    {
      type: 'dictionary',
      searchValue: '1',
      path: '{}.id',
      inputDictionary: this.booksDictionary,
      outputKey: `'name'`,
      description: `Lookup object by ID in a map, and return it's 'name' property.`,
      flow: `1 => 'Name'`,
    },
  ];

  getExampleTypes() {
    return this.exampleTypes;
  }

  getExamples() {
    return this.examples;
  }

  getExample(exampleId: number) {
    return this.examples[exampleId];
  }

  constructor() { }
}
